import React, { Component } from 'react';
import './App.css';
import Column from './components/column';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

import Button from "react-bootstrap/Button";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>
            Retrospect
          </p>
        </header>

        <Container>
          <Row>
            <Col>
              <h1>Start</h1>
              What should the team start doing?
            </Col>
            <Col>
              <h1>Stop</h1>
              What should the team stop doing?
            </Col>
            <Col>
              <h1>Continue</h1>
              What should the team continue doing?
            </Col>
          </Row>
        </Container>

        <Button>Add Note</Button>


        <script src="https://unpkg.com/react/umd/react.production.js" crossOrigin/>
        <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
            crossOrigin="anonymous"
        />

      </div>
    );
  }
}

export default App;
